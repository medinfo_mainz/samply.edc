/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Corrects shorttext date strings to the format MM/YYYY Years < 50 will be
 * transformed to 2000+year Years < 50 and < 100 will be transformed to
 * 1900+year Months get a leading zero if necessary.
 */
public class MonthYearDateConverter implements Converter {

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext
     * , javax.faces.component.UIComponent, java.lang.String)
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component,
            String value) {

        if (value == null || value.equals(""))
            return null;

        Integer month = 0;
        Integer year = 0;

        try {
            String temp = value.toString();
            temp = temp.replace("_", "");

            String[] parts = temp.split("/");
            month = new Integer(parts[0]);
            year = new Integer(parts[1]);

        } catch (Exception e) {
        }

        if (year < 50)
            year += 2000;
        else if (year >= 50 && year < 100)
            year += 1900;

        String ret = "";
        if (month < 10)
            ret += "0" + month;
        else
            ret += month;

        ret += "/" + year;
        return ret;

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext
     * , javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component,
            Object value) {
        if (value == null)
            return null;

        return value.toString();

    }
}