/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.control;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;

import de.samply.edc.utils.Utils;

/**
 * Control class for mail sending.
 */
public class Mailer {

    /**
     * Sends an email.
     *
     * @param subject
     *            the subject
     * @param text
     *            the text
     * @param receivers
     *            the receivers
     * @return sent or not
     */
    public static Boolean sendEmail(String subject, String text,
            String[] receivers) {

        String receiversString = StringUtils.join(receivers, ",");
        Configuration config = Utils.getAB().getConfig();

        Properties props = new Properties();
        props.put("mail.smtp.host", config.getString("mail.smtp.host"));
        props.put("mail.smtp.user", config.getString("mail.smtp.user"));

        Session session = Session.getInstance(props, null);

        try {
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(config.getString("mail.from"),
                    config.getString("mail.sendername")));
            msg.setRecipients(Message.RecipientType.TO, receiversString);
            msg.setText(text);
            msg.setSentDate(new Date());
            msg.setSubject(subject);
            Transport.send(msg);
        } catch (MessagingException mex) {
            System.err.println("Der Mailversand ist fehlgeschlagen."
                    + mex.toString());
            return false;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return true;
    }
}
