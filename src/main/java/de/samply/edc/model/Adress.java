/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Adress model class.
 */
public class Adress implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**  The street, location (=city), plz (=postal code). */
    private String street, location, plz;

    /**
     * Gets the street.
     *
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets the street.
     *
     * @param street
     *            the new street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Gets the location/city.
     *
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the location/city.
     *
     * @param location            the new location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * Gets the plz/postal code.
     *
     * @return the plz/postal code
     */
    public String getPlz() {
        return plz;
    }

    /**
     * Sets the plz/postal code.
     *
     * @param plz            the new plz/postal code
     */
    public void setPlz(String plz) {
        this.plz = plz;
    }

    /**
     * Instantiates a new adress.
     *
     * @param entries
     *            the adress data as hashmap with keys street, plz, location
     */
    public Adress(HashMap<String, Object> entries) {
        street = (String) entries.get("street");
        plz = (String) entries.get("plz");
        location = (String) entries.get("location");
    }

    /**
     * Instantiates a new address.
     *
     * @param street
     *            the street
     * @param plz
     *            the plz
     * @param location
     *            the location
     */
    public Adress(String street, String plz, String location) {
        this.street = street;
        this.plz = plz;
        this.location = location;
    }

    /**
     * Instantiates a new empty address.
     */
    public Adress() {
        street = "";
        plz = "";
        location = "";
    }
}
