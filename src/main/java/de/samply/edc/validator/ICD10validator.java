/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * JSF validator for ICD10 code.
 */
@FacesValidator("de.samply.bmb.utils.ICD10validator")
public class ICD10validator implements Validator {

    /** The Constant ICD10_PATTERN. */
    private static final String ICD10_PATTERN = "^[a-zA-Z]" + "[0-9]{2}"
            + "\\." + "[0-9]{1,2}" + "$";

    /**
     * Instantiates a new ICD10validator.
     */
    public ICD10validator() {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.faces.validator.Validator#validate(javax.faces.context.FacesContext
     * , javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {
        Boolean ok = false;

        if (value != null && !value.toString().equals("")) {
            Pattern pattern = Pattern.compile(ICD10_PATTERN);
            Matcher matcher = pattern.matcher(value.toString());
            ok = matcher.matches();
        }

        if (!ok) {
            String summary = "Kein gültiger ICD10 Code.";

            FacesMessage message = new FacesMessage();
            message.setSummary(summary);
            message.setDetail("Kein gültiger ICD10 Code.\n");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }

    }
}