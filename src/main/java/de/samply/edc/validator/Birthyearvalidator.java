/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.validator;

import java.util.Calendar;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import de.samply.edc.utils.Utils;

/**
 * JSF Validator for birthyears.
 * Checks if a birthyear is in the future or more than 100 years ago
 */
@FacesValidator("de.samply.bmb.utils.Birthyearvalidator")
public class Birthyearvalidator implements Validator {

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.faces.validator.Validator#validate(javax.faces.context.FacesContext
     * , javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {
        String value_s = (String) value;

        if (context.getPartialViewContext().isPartialRequest()) {
            return;
        }

        if (Calendar.getInstance().get(Calendar.YEAR) < Integer
                .parseInt(value_s)) {
            String summary = Utils.translate("validator_yob_future");

            FacesMessage message = new FacesMessage();
            message.setSummary(summary);
            message.setDetail(Utils.translate("validator_yob_future") + "\n");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
        if (Calendar.getInstance().get(Calendar.YEAR) - 100 > Integer
                .parseInt(value_s)) {
            String summary = Utils.translate("validator_yob_100");

            FacesMessage message = new FacesMessage();
            message.setSummary(summary);
            message.setDetail(Utils.translate("validator_yob_100") + "\n");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }

    }
}