/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.validator;

import java.util.Calendar;
import java.util.MissingResourceException;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import de.samply.edc.utils.Utils;

/**
 * JSF validator for MonthYear strings.
 */
public class MonthYearDate implements Validator {

    /**
     * Throw error message.
     *
     * @param myReason
     *            The error message.
     */
    public void throwMessage(String myReason) {
        String summary = "";

        try {
            summary = Utils.getResourceBundleString("validation_error_header");
            if (myReason == null) {
                myReason = Utils.getResourceBundleString("date_error_format");
            }
        } catch (MissingResourceException e) {
            Utils.getLogger().debug("Missing resource!");
        }

        FacesMessage message = new FacesMessage();
        message.setSummary(summary);
        message.setDetail(myReason);
        message.setSeverity(FacesMessage.SEVERITY_ERROR);

        throw new ValidatorException(message);
    }

    /**
     * Throw message that date is in the future.
     */
    public void throwFutureMessage() {
        String myReason = "";
        try {
            myReason = Utils.getResourceBundleString("date_error_future");
        } catch (MissingResourceException e) {
        }

        throwMessage(myReason);
    }

    /**
     * Throw that date is too far in the past.
     */
    private void throwPastMessage() {
        String reason = "";
        try {
            reason = Utils.getResourceBundleString("date_error_past");
        } catch (MissingResourceException e) {
        }

        throwMessage(reason);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.faces.validator.Validator#validate(javax.faces.context.FacesContext
     * , javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {

        Integer month = null;
        String monthString = "";
        Integer year = null;

        try {
            String temp = value.toString();
            temp = temp.replace("_", "");

            String[] parts = temp.split("/");
            monthString = parts[0];
            month = new Integer(parts[0]);
            year = new Integer(parts[1]);

        } catch (Exception e) {
            throwMessage(null);
        }

        if (monthString.length() < 2)
            throwMessage(null);

        if (month < 1 || month > 12)
            throwMessage(null);

        if (year < 50)
            year += 2000;
        else if (year >= 50 && year < 100)
            year += 1900;

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        if (year > currentYear)
            throwFutureMessage();

        if (year < 1900)
            throwPastMessage();

    }
}