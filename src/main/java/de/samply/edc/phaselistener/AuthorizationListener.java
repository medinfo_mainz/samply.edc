/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.phaselistener;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

import de.samply.edc.control.AbstractSessionBean;
import de.samply.edc.utils.Utils;

/**
 * The listener interface for receiving authorization events. The class that is
 * interested in processing an authorization event implements this interface, and
 * the object created with that class is registered with a component using the
 * component's <code>addAuthorizationListener<code> method. When
 * the authorization event occurs, that object's appropriate
 * method is invoked.
 * 
 * This listener is used to ensure that pages are only displayed
 * if a user is logged into the system.
 * If not, it will refer back to the login page.
 *
 * @see AuthorizationEvent
 */
public class AuthorizationListener implements PhaseListener {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
     */
    @Override
    public void afterPhase(PhaseEvent event) {

        FacesContext facesContext = event.getFacesContext();

        boolean isLoginPage = Utils.isCurrentPage("login.xhtml", facesContext);
        boolean isnoJSPage = Utils.isCurrentPage("nojavascript.xhtml",
                facesContext);
        boolean isSearchViewPage = Utils.isCurrentPage("patientlistSearchView.xhtml", facesContext);

        // do not check for a valid login on certain pages
        if (Utils.isCurrentPage("activate.xhtml", facesContext)
                || Utils.isCurrentPage("test.xhtml", facesContext)
                || Utils.isCurrentPage("forgotpassword.xhtml", facesContext)
                || isSearchViewPage) {
            return;
        }

        HttpSession session = (HttpSession) facesContext.getExternalContext()
                .getSession(false);
        AbstractSessionBean sessionBean = (AbstractSessionBean) Utils
                .getBackingBean("sessionBean", facesContext);

        if (session == null) {
            sessionBean.getDatabase().logout();

            Utils.navigateToPage("loginPage", facesContext, false);
        }

        else {
            if (!isLoginPage && !isnoJSPage
                    && sessionBean.getCurrentObject("user") == null) {
                sessionBean.getDatabase().logout();
                Utils.navigateToPage("loginPage", facesContext, false);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.faces.event.PhaseListener#beforePhase(javax.faces.event.PhaseEvent)
     */
    @Override
    public void beforePhase(PhaseEvent event) {

    }

    /*
     * (non-Javadoc)
     *
     * @see javax.faces.event.PhaseListener#getPhaseId()
     */
    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

}
