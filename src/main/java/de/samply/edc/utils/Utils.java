/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.edc.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.DefaultConfigurationBuilder;
import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import com.google.template.soy.SoyFileSet;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.tofu.SoyTofu;

import de.samply.common.http.HttpConnector43;
import de.samply.config.util.FileFinderUtil;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractApplicationBean;
import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.control.AbstractSessionBean;
import de.samply.edc.exceptions.PasswordComplexityException;
import de.samply.edc.listener.StartupListener;
import de.samply.edc.osse.handler.XMLErrorHandler;
import de.samply.edc.validator.PasswordComplexityValidator;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;

/**
 * A general static helper class.
 */
public class Utils implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 44636L;

    /** The log4j logger. */
    private static Logger logger = LogManager.getLogger("de.samply.edc");

    /**
     * Gets the logger.
     *
     * @return the logger
     */
    public static Logger getLogger() {
        return logger;
    }

    /**
     * Update log4j log dir.
     *
     * @param logDir
     *            the log dir
     */
    // TODO: FIX this for log4j V2
    public static void updateLog4jLogDir(String logDir) {
        Properties props = new Properties();
        try {
            InputStream configStream = Utils.class.getResourceAsStream("/log4j.properties");
            props.load(configStream);
            configStream.close();
        } catch (IOException e) {
            System.err.println("Error: Cannot load configuration file ");
        }
        props.setProperty("log4j.appender.FILE.file", logDir + "de.samply.edc.log");
        // LogManager.resetConfiguration();
        // PropertyConfigurator.configure(props);
    }

    /**
     * The edit mode for forms.
     */
    public enum editMode {
        
        /**  Edit not allowed (read-only). */
        EDIT_OFF(0),
        
        /**  Edit allowed. */
        EDIT_ON(1),
        
        /** Edit mode is implicit, which means the form is presented with input forms and it initiates the edit mode if any value is changed. */
        EDIT_IMPLICIT(2),
        /** Form is newly created. */
        EDIT_NEW(3);

        /** The value. */
        private final int value;

        /**
         * Instantiates a new edit mode from an integer.
         *
         * @param value
         *            the value
         */
        private editMode(int value) {
            this.value = value;
        }

        /**
         * Gets the value as integer.
         *
         * @return the value
         */
        public int getValue() {
            return this.value;
        }

        /**
         * Gets the enum by value.
         *
         * @param value
         *            the value
         * @return the enum by value
         */
        public static editMode getEnumByValue(int value) {
            for (editMode em : editMode.values()) {
                if (em.getValue() == value) {
                    return em;
                }
            }
            return null;
        }

        /*
         * (non-Javadoc)
         *
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            switch (this) {
            case EDIT_OFF:
                return "";
            case EDIT_ON:
                return "editMode";
            case EDIT_IMPLICIT:
                return "editModeImplicit";
            case EDIT_NEW:
                return "editModeCreate";

            default:
                return "";
            }
        }
    }

    /**
     * Calculate age based on the year of birth and a date.
     *
     * @param yearOfBirth            the year of birth
     * @param caseDate            the case date
     * @return the int
     */
    public static int calculateAge(String yearOfBirth, Date caseDate) {
        int birth;
        int ret;

        if (caseDate == null || yearOfBirth == null || yearOfBirth.length() < 4)
            return 0;

        try {
            birth = Integer.parseInt(yearOfBirth);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(caseDate);
            ret = calendar.get(Calendar.YEAR) - birth;
        } catch (NumberFormatException e1) {
            Utils.getLogger().debug("Couldn't calculate Age");
            ret = 0;
        }
        if (ret < 0)
            return 0;
        return ret;
    }

    /**
     * Gets the sessionBean.
     *
     * @return the sessionBean
     */
    public static AbstractSessionBean getSB() {
        return (AbstractSessionBean) FacesContext.getCurrentInstance().getApplication().getELResolver()
                .getValue(FacesContext.getCurrentInstance().getELContext(), null, "sessionBean");
    }

    /**
     * Gets the applicationBean.
     *
     * @return the applicationBean
     */
    public static AbstractApplicationBean getAB() {
        return (AbstractApplicationBean) FacesContext.getCurrentInstance().getApplication().getELResolver()
                .getValue(FacesContext.getCurrentInstance().getELContext(), null, "applicationBean");
    }

    /**
     * Finds a bean in the given context, or the current instance if null.
     *
     * @param <T> the generic type
     * @param beanName            The bean's name
     * @param context            if null, current instance is used
     * @return the t
     */
    @SuppressWarnings("unchecked")
    public static <T> T findBean(String beanName, FacesContext context) {
        if (context == null)
            context = FacesContext.getCurrentInstance();
        return (T) context.getApplication().evaluateExpressionGet(context, "#{" + beanName + "}", Object.class);
    }

    /**
     * Gets the resource bundle text of a certain key.
     *
     * @param key            the key
     * @param ignoreMissing            do not display ??? around the key, if the entry is missing in
     *            the resource bundle
     * @return the resource bundle text
     */
    public static String getResourceText(String key, boolean ignoreMissing) {
        String text;
        FacesContext ctx = FacesContext.getCurrentInstance();
        Locale locale = ctx.getViewRoot().getLocale();

        String bundleName = "de.samply.edc.messages.messages";

        try {
            ResourceBundle bundle = ResourceBundle.getBundle(bundleName, locale);
            text = bundle.getString(key);
        } catch (MissingResourceException e) {
            if (ignoreMissing == true)
                return key;
            else
                return "???" + key + "???";
        }
        return text;
    }

    /**
     * Gets the resource bundle text of a given key.
     *
     * @param key            the key
     * @return the resource text
     */
    public static String getResourceText(String key) {
        return getResourceText(key, false);
    }

    /**
     * Gets a backing bean of a given name.
     *
     * @param beanName            the bean name
     * @return the backing bean
     */
    public static Object getBackingBean(String beanName) {
        return getBackingBean(beanName, null);
    }

    /**
     * Gets a backing bean of a given name in a FacesContext.
     *
     * @param beanName            the bean name
     * @param context            the FacesContext
     * @return the backing bean
     */
    public static Object getBackingBean(String beanName, FacesContext context) {
        if (context == null)
            context = FacesContext.getCurrentInstance();
        return context.getApplication().getELResolver().getValue(context.getELContext(), null, beanName);
    }

    /**
     * Gets the database wrapper.
     *
     * @return the database wrapper
     */
    public static AbstractDatabase<?> getDatabase() {
        return ((AbstractSessionBean) getBackingBean("sessionBean")).getDatabase();
    }

    /**
     * Gets the date string in a given format.
     *
     * @param date            the date
     * @param format            the format (if null it defaults to yyyy-MM-dd hh:mm:ss)
     * @return the date string
     */
    public static String getDateString(Date date, String format) {
        if (date == null)
            return "";

        if (format == null || "".equals(format))
            format = "yyyy-MM-dd hh:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String formattedDate = sdf.format(date);

        return formattedDate;
    }

    /**
     * Gets the version of a form.
     *
     * @param currentForm            the current form
     * @return the form version
     */
    public static Integer getFormVersion(String currentForm) {
        HashMap<String, Object> formVersionData = getFormVersionData(currentForm);
        if (formVersionData != null)
            return (Integer) formVersionData.get("version");

        return 1;
    }

    /**
     * Check if a form is special.
     *
     * @param currentForm            the current form
     * @param specialty            the specialty
     * @return the string
     */
    public static String checkIsSpecialForm(String currentForm, String specialty) {
        if (currentForm == null)
            return null;

        String[] cut = currentForm.split("_");
        if (cut.length < 2) {
            // Not a versionized form
            return null;
        }

        if (cut[0].equals(specialty)) {
            return currentForm.substring(specialty.length() + 1);
        }

        return null;
    }

    /**
     * Gets the page as entered in the configuration (defaults to fallbackpage).
     *
     * @param page            the page
     * @return the page
     */
    public static String getPage(String page) {
        return getAB().getConfig().getString("form." + page, getFallbackPage());
    }

    /**
     * Gets the patient main page.
     *
     * @return the patient main page
     */
    @Deprecated
    public static String getPatientMainPage() {
        return getAB().getConfig().getString("form.patient");
    }

    /**
     * Gets the visit main page.
     *
     * @return the visit main page
     */
    @Deprecated
    public static String getVisitMainPage() {
        return getAB().getConfig().getString("form.visit");
    }

    /**
     * Gets the fallback page .
     *
     * @return the fallback page
     */
    @Deprecated
    public static String getFallbackPagePure() {
        return "patientlist";
    }

    /**
     * Gets the fallback page.
     *
     * @return the fallback page
     */
    public static String getFallbackPage() {
        return "patientlist";
    }

    /**
     * Gets the group page.
     *
     * @return the group page
     */
    public static String getGroupPage() {
        return getAB().getConfig().getString("form.group");
    }

    /**
     * Gets the group patients page.
     *
     * @return the group patients page
     */
    public static String getGroupPatientsPage() {
        return getAB().getConfig().getString("form.groupPatients");
    }

    /**
     * Gets the group samples page.
     *
     * @return the group samples page
     */
    public static String getGroupSamplesPage() {
        return getAB().getConfig().getString("form.groupSamples");
    }

    /**
     * Gets the group queries page.
     *
     * @return the group queries page
     */
    public static String getGroupQueriesPage() {
        return getAB().getConfig().getString("form.groupQueries");
    }

    /**
     * Gets the views page.
     *
     * @return the views page
     */
    public static String getViewsPage() {
        return getAB().getConfig().getString("form.views");
    }

    /**
     * Gets the search page.
     *
     * @return the search page
     */
    public static String getSearchPage() {
        return getAB().getConfig().getString("form.search");
    }

    /**
     * Gets the pure form name (form names consist of the pure name, a "-", and
     * its version, for example: foobar-3.
     *
     * @param currentForm            the current form
     * @return the form name pure
     */
    public static String getFormNamePure(String currentForm) {
        if (currentForm == null)
            return null;

        String[] cut = currentForm.split("-");
        if (cut.length < 2) {
            // Not a versionized form
            return currentForm;
        }

        return cut[0];
    }

    /**
     * Gets the form version data.
     *
     * @param currentForm
     *            the current form
     * @return the form version data
     */
    public static HashMap<String, Object> getFormVersionData(String currentForm) {
        if (currentForm == null)
            return null;

        Integer currentFormVersion = null;

        String[] cut = currentForm.split("-");
        if (cut.length < 2) {
            // Not a versionized form
            return null;
        }

        try {
            currentFormVersion = new Integer(cut[cut.length - 1]);
        } catch (Exception e) {
            // wups no integer? So the form name was illegal. Let's get out of
            // here.
            return null;
        }

        String currentFormName = "";
        for (int i = 0; i < cut.length - 1; i++) {
            currentFormName += cut[i];
        }

        HashMap<String, Object> ret = new HashMap<String, Object>();
        ret.put("version", currentFormVersion);
        ret.put("name", currentFormName);

        return ret;
    }

    /**
     * Creates an integer array from begin to end.
     *
     * @param begin            starting number
     * @param end            ending number
     * @return integer[]
     */
    public static Integer[] createIntegerArray(int begin, int end) {
        int direction = (begin < end) ? 1 : (begin > end) ? -1 : 0;
        int size = Math.abs(end - begin) + 1;
        Integer[] array = new Integer[size];

        for (int i = 0; i < size; i++) {
            array[i] = begin + (i * direction);
        }

        return array;
    }

    /**
     * Generates a random string.
     *
     * @param length
     *            the length
     * @param onlybiggies
     *            only use uppercase chars
     * @return the string
     */
    public static String generateRandomString(int length, boolean onlybiggies) {
        String alphabet;
        if (onlybiggies) {
            alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        } else {
            alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        }

        return generateString(alphabet, length);
    }

    /**
     * Generates a random string.
     *
     * @param characters
     *            the characters the string should be formed by
     * @param length
     *            the length of the string
     * @return the string
     */
    public static String generateString(String characters, int length) {
        SecureRandom rng = new SecureRandom();
        char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }
        return new String(text);
    }

    /**
     * Generates a string with characters that are unlikely to form an actual
     * word and that are easily distinguishable.
     * 
     * @param length
     *            length of returned string
     * @param lowercaseOnly
     *            use only lowercase characters
     * @return generated string
     */
    public static String generateRandomStringForUser(int length, boolean lowercaseOnly) {
        String alphabet = "bcdfghkmnpqrstvwxyz";
        if (!lowercaseOnly) {
            alphabet += "BCDFGHKLMNPQRSTVWXYZ";
        }
        return generateString(alphabet, length);
    }

    /**
     * Gets the basedir of an web URL.
     *
     * @return the basedir
     */
    public static String getBasedir() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        String contextURL = request.getRequestURL().toString().replace(request.getRequestURI().substring(0), "")
                + request.getContextPath() + "/";
        return contextURL;
    }

    /**
     * Reloads a page.
     *
     * @param context
     *            the context
     */
    public static void reloadPage(FacesContext context) {
        if (context == null)
            context = FacesContext.getCurrentInstance();

        String viewId = context.getViewRoot().getViewId();
        UIViewRoot root = context.getApplication().getViewHandler().createView(context, viewId);
        root.setViewId(viewId);
        context.setViewRoot(root);
    }

    /**
     * Gets the session timeout in minutes.
     *
     * @return the session timeout in min
     */
    public static int getSessionTimeoutMin() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        return (session.getMaxInactiveInterval() / 60);
    }

    /**
     * Refreshes a session.
     */
    public static void refreshSession() {
        // In filters FacesContext may not be there, so then get session from
        // request instead
        // ((HttpServletRequest) request).getSession() or
        // HttpRequest.getSession(boolean)
        FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    }

    /**
     * Sets a session value.
     *
     * @param key
     *            the key
     * @param value
     *            the value
     */
    public static void setSessionValue(String key, Object value) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(key, value);
    }

    /**
     * Gets a session value.
     *
     * @param key
     *            the key
     * @return the session value
     */
    public static Object getSessionValue(String key) {
        // return ((HttpSession)
        // FacesContext.getCurrentInstance().getExternalContext().getSession(false)).getAttribute("username");
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(key);
    }

    /**
     * Removes a session value.
     *
     * @param key
     *            the key
     */
    public static void removeSessionValue(String key) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(key);
    }

    /**
     * Kills the complete session.
     */
    public static void killSession() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    }

    /**
     * Gets the default locale.
     *
     * @return the default locale
     */
    public static Locale getDefaultLocale() {
        if (FacesContext.getCurrentInstance().getApplication().getDefaultLocale() == null)
            return new Locale("en");

        return FacesContext.getCurrentInstance().getApplication().getDefaultLocale();
    }

    /**
     * Gets the current locale.
     *
     * @return the locale
     */
    public static Locale getLocale() {
        if (FacesContext.getCurrentInstance().getViewRoot().getLocale() == null)
            return new Locale("en");
        return FacesContext.getCurrentInstance().getViewRoot().getLocale();
    }

    /**
     * Gets the current language.
     *
     * @return ISO-639 language code
     */
    public static String getLanguage() {
        return FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
    }

    /**
     * Sets the current {@code Locale} for each user session.
     *
     * @param languageCode
     *            - ISO-639 language code
     */
    public static void changeLanguage(String languageCode) {
        FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(languageCode));
    }

    /**
     * Adds a context message to JSF (mostly used for growl messages).
     *
     * @param subject            the subject
     * @param message            the message
     */
    public static void addContextMessage(String subject, String message) {
        addContextMessage(null, subject, message);
    }

    /**
     * Adds a context message to JSF (mostly used for growl messages).
     *
     * @param subject            the subject
     * @param message            the message
     * @param severity            the severity
     */
    public static void addContextMessage(String subject, String message, Severity severity) {
        addContextMessage(null, subject, message, severity);
    }

    /**
     * Adds a context message to JSF (mostly used for growl messages).
     *
     * @param clientID            the client id
     * @param subject            the subject
     * @param message            the message
     * @param severity            the severity
     */
    public static void addContextMessage(String clientID, String subject, String message, Severity severity) {
        FacesContext.getCurrentInstance().addMessage(clientID, new FacesMessage(severity, subject, message));
    }

    /**
     * Adds a context message to JSF (mostly used for growl messages).
     *
     * @param clientID            the client id
     * @param subject            the subject
     * @param message            the message
     */
    public static void addContextMessage(String clientID, String subject, String message) {
        FacesContext.getCurrentInstance().addMessage(clientID,
                new FacesMessage(FacesMessage.SEVERITY_INFO, subject, message));
    }

    /**
     * Returns a localized bundlemessage string that has parameters to be
     * replaced.
     *
     * @param messageFile
     *            null| bundle file
     * @param key
     *            key of the message
     * @param params
     *            Object[] array of params to replace in the message
     * @return String the localized text
     */
    public static String getResourceBundleString(String messageFile, String key, Object params[]) {
        String text;
        ResourceBundle bundle;

        if (messageFile == null) {
            bundle = getResourceBundle();
        } else {
            bundle = getResourceBundle(messageFile);
        }

        try {
            text = bundle.getString(key);
        } catch (MissingResourceException e) {
            text = "?? key " + key + " not found ??";
        }

        if (params != null) {
            MessageFormat mf = new MessageFormat(text, getLocale());
            text = mf.format(params, new StringBuffer(), null).toString();
        }

        return text;
    }

    /**
     * Gets the resource bundle string of a key and replaces parameters in it
     * with the given params.
     *
     * @param key
     *            the key
     * @param params
     *            the params
     * @return the resource bundle string
     */
    public static String getResourceBundleString(String key, Object params[]) {
        String text;
        ResourceBundle bundle = getResourceBundle();

        try {
            text = bundle.getString(key);
        } catch (MissingResourceException e) {
            text = "?? key " + key + " not found ??";
        }

        if (params != null) {
            MessageFormat mf = new MessageFormat(text, getLocale());
            text = mf.format(params, new StringBuffer(), null).toString();
        }

        return text;
    }

    /**
     * Gets the resource bundle string of a key.
     *
     * @param messageFile            the message file
     * @param key            the key
     * @return the resource bundle string
     */
    public static String getResourceBundleString(String messageFile, String key) {
        return getResourceBundle(messageFile).getString(key);
    }

    /**
     * Gets the resource bundle string.
     *
     * @param key
     *            the key
     * @return the resource bundle string
     */
    public static String getResourceBundleString(String key) {
        return getResourceBundle().getString(key);
    }

    /**
     * Gets the standard resource bundle.
     *
     * @return the resource bundle de.samply.edc.messages.messages
     */
    public static ResourceBundle getResourceBundle() {
        return getResourceBundle("de.samply.edc.messages.messages");
    }

    /**
     * Gets the resource bundle.
     *
     * @param messageFile
     *            the message file
     * @return the resource bundle
     */
    public static ResourceBundle getResourceBundle(String messageFile) {
        return ResourceBundle.getBundle(messageFile, getLocale());
    }

    /**
     * Gets the resource bundle string with placeholders.
     *
     * @param messageKey
     *            the message key
     * @param replaceArray
     *            the replace array
     * @return the resource bundle string with placeholders
     */
    public static String getResourceBundleStringWithPlaceholders(String messageKey, String[] replaceArray) {
        return getResourceBundleStringWithPlaceholders(messageKey, replaceArray, null);
    }

    /**
     * Gets the resource bundle string with placeholders.
     *
     * @param messageKey
     *            the message key
     * @param replaceArray
     *            the replace array
     * @param messageFile
     *            the message file
     * @return the resource bundle string with placeholders
     */
    public static String getResourceBundleStringWithPlaceholders(String messageKey, String[] replaceArray,
            String messageFile) {
        String message = "";
        if (messageFile == null)
            message = Utils.getResourceBundleString(messageKey);
        else
            message = Utils.getResourceBundleString(messageFile, messageKey);
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        MessageFormat mf = new MessageFormat(message, locale);
        message = mf.format(replaceArray, new StringBuffer(), null).toString();
        return message;
    }

    /**
     * Recursive searcher to find the HTML-Component "id" among the children of
     * "base".
     *
     * @param base
     *            UIComponent - the father
     * @param id
     *            String - the ID to look for
     * @return UIComponent
     */
    public static UIComponent findComponent(UIComponent base, String id) {
        if (id.equals(base.getId()))
            return base;

        UIComponent kid = null;
        UIComponent result = null;
        Iterator<UIComponent> kids = base.getFacetsAndChildren();
        while (kids.hasNext() && (result == null)) {
            kid = kids.next();
            if (id.equals(kid.getId())) {
                result = kid;
                break;
            }
            result = findComponent(kid, id);
            if (result != null) {
                break;
            }
        }
        return result;
    }

    /**
     * Finds a component on the page.
     *
     * @param id
     *            ID of the HTML-Component to find
     * @return UIComponent
     */
    public static UIComponent findComponentInRoot(String id) {
        UIComponent component = null;

        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext != null) {
            UIComponent root = facesContext.getViewRoot();
            component = findComponent(root, id);
        }

        return component;
    }

    /**
     * Gets the HTTP request.
     *
     * @param key
     *            the key
     * @return the HTTP request
     */
    public static String getHTTPRequest(String key) {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
    }

    /**
     * Checks if the current page is what you want it to be.
     *
     * @param page
     *            Page name like "login.xhtml"
     * @param context
     *            The context, can be null then
     *            FacesContext.getCurrentInstance() will be used
     * @return the boolean
     */
    public static Boolean isCurrentPage(String page, FacesContext context) {
        if (context == null)
            context = FacesContext.getCurrentInstance();

        String currentPage = context.getViewRoot().getViewId();
        return (currentPage.lastIndexOf(page) > -1);
    }

    /**
     * Two different methods to navigate/redirect
     *
     * url can be a navigation "outcome" (see faces-config) or a page like
     * "login.xhtml"
     *
     * @param url
     *            URL (or outcome) to go to
     * @param context
     *            The context, can be null then
     *            FacesContext.getCurrentInstance() will be used
     * @param redirect
     *            Make it a redirect navigation
     */
    public static void navigateToPage(String url, FacesContext context, Boolean redirect) {
        if (redirect)
            url += "?faces-redirect=true";

        if (context == null)
            context = FacesContext.getCurrentInstance();

        context.getApplication().getNavigationHandler().handleNavigation(context, null, url);
    }

    /**
     * Go to a form templated by index.xhtml
     *
     * @param form
     *            the form
     */
    public static void goForm(String form) {
        String page = "index.xhtml?form=" + form;

        try {
            redirectToPage(page);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Go admin form templated by admin.xhtml
     *
     * @param form
     *            the form
     */
    public static void goAdminForm(String form) {
        String page = "admin.xhtml?form=" + form;

        try {
            redirectToPage(page);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Redirect to a page.
     *
     * @param url
     *            the url
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void redirectToPage(String url) throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }

    /**
     * Who called me, to find out which method called another method.
     *
     * @return returns the method name that called "me"
     */
    public static String whoCalledMe() {
        StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
        // numbers:
        // 1 = me
        // 2 = whocalledme (so the method which called Utils.whoCalledMe)
        // 3 = the one who called whocalledme :-)
        // we return 3 ofc
        StackTraceElement e = stacktrace[3];// maybe this number needs to be
                                            // corrected

        Utils.getLogger().debug(stacktrace[2] + " was called by " + stacktrace[3]);

        return e.getMethodName();
    }

    /**
     * Sha512 of a salted password string.
     *
     * @param password            the password
     * @param salt            the salt
     * @return the Sha512
     * @throws NoSuchAlgorithmException             the no such algorithm exception
     */
    public static String sha512(String password, String salt) throws NoSuchAlgorithmException {
        String ret = "";
        if (password == null || password.equals(""))
            return "";

        password = password + salt;

        MessageDigest md;
        md = MessageDigest.getInstance("SHA-512");
        md.update(password.getBytes());

        byte byteData[] = md.digest();

        // convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        ret = sb.toString();

        return ret;
    }

    /**
     * Gets the unique id of object.
     *
     * @param ob
     *            the ob
     * @return the unique id of object
     */
    public static int getUniqueIdOfObject(Object ob) {
        return System.identityHashCode(ob);
    }

    /**
     * Gets the client ip.
     *
     * @return the client ip
     */
    public static String getClientIP() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }

        return ipAddress;
    }

    /**
     * Gets the real path (in the system) of a relative web path.
     *
     * @param relativeWebPath            the relative web path
     * @return the real path
     */
    public static String getRealPath(String relativeWebPath) {
        if (FacesContext.getCurrentInstance() == null)
            return null;

        ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        return sc.getRealPath(relativeWebPath);
    }

    /**
     * Gets the value of an init parameter.
     *
     * @param param
     *            the param
     * @return the set value
     */
    public static String getInitParameter(String param) {
        ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        return sc.getInitParameter(param);
    }

    /**
     * Finds a configuration file. this method finds a certain file in
     * predefined directories in the system
     *
     * 1) System ENV (OSSEConfigurationPath) 2) Init Param
     * (OSSEConfigurationPath) 3) OS based: 3.1) Linux: /etc/osse/ 3.2) Windows:
     * Registry entry HKLM\Software\OSSE\ConfDir 4) Fallback: WEB-INF/classes/
     *
     * @param filename
     *            the filename to find
     * @return the string
     */
    public static String findConfigurationFile(String filename) {
        if (filename == null)
            return null;

        try {
            File file = FileFinderUtil.findFile(filename, StartupListener.getProjectName(), getRealPath("/WEB-INF"));
            return file.getAbsolutePath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Finds the configuration path in which the file "config.xml" resides
     *
     * 1) System ENV (OSSEConfigurationPath) 2) Init Param
     * (OSSEConfigurationPath) 3) OS based: 3.1) Linux: /etc/osse/ 3.2) Windows:
     * Registry entry HKLM\Software\OSSE\ConfDir 4) Fallback: WEB-INF/classes/
     *
     * @return the configuration path
     */
    public static String findConfigurationPath() {

        String fileName = "config.xml";
        try {
            File file = FileFinderUtil.findFile(fileName, StartupListener.getProjectName(), getRealPath("/WEB-INF"));
            String absolutePath = file.getAbsolutePath();
            String filePath = absolutePath.substring(0, absolutePath.lastIndexOf(File.separator));
            String configPath = filePath + File.separator;

            logger.info("Found configfile in path " + configPath);
            return configPath;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        logger.info("Did not find configfile.");
        return "";
    }

    /**
     * Gets the configuration builder for the file config.xml
     *
     * @return the configuration builder
     */
    public static DefaultConfigurationBuilder getConfigurationBuilder() {
        try {
            return new DefaultConfigurationBuilder(findConfigurationFile("config.xml"));
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Loads the apache configuration from the config.xml
     *
     * @return the configuration
     */
    public static Configuration getConfig() {
        Configuration config = null;

        try {
            DefaultConfigurationBuilder builder = new DefaultConfigurationBuilder(findConfigurationFile("config.xml"));
            config = builder.getConfiguration(true);
        } catch (ConfigurationException e) {
            Utils.getLogger().debug("initialize() config exception: " + e);
        } catch (Error e) {
            Utils.getLogger().debug("initialize() config error: " + e);
        }

        return config;
    }

    /**
     * Upper case first a string.
     *
     * @param text            the text
     * @return the string
     */
    public static String upperCaseFirst(String text) {
        String lastPart = text.substring(1);
        String firstLetter = text.substring(0, 1);
        firstLetter = firstLetter.toUpperCase();
        return (firstLetter + lastPart);
    }

    /**
     * Sort by date (format dd.MM.yyyy)
     *
     * @param s1
     *            the s1
     * @param s2
     *            the s2
     * @return the int
     */
    public static int sortByDate(String s1, String s2) {
        Date d1;
        Date d2;
        try {
            d1 = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN).parse(s1);
        } catch (ParseException e) {
            return -1;
        }
        try {
            d2 = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN).parse(s2);
        } catch (ParseException e) {
            return 1;
        }

        if (d1.before(d2))
            return -1;
        if (d1.after(d2))
            return 1;
        return 0;
    }

    /**
     * Sort by date (format dd.MM.yyyy).
     *
     * @param o1
     *            the o1
     * @param o2
     *            the o2
     * @return the int
     */
    public static int sortByDate(Object o1, Object o2) {
        return sortByDate((String) o1, (String) o2);
    }

    /**
     * Translate a special resource key and return its bundle message, which may
     * be ':'-seperated list of keys (in which case it returns the message of
     * the last key).
     *
     * @param in            the in
     * @return the string
     */
    public static String translate(String in) {
        if (in == null || in.length() < 1)
            return "";
        if (in.contains(":"))
            return Utils.getResourceText(in.substring(in.indexOf(':') + 1), true);

        return Utils.getResourceText(in, true);
    }

    /**
     * Translate a special resource key and return its bundle message, which may
     * be space-seperated list of keys.
     *
     * @param in            the in
     * @return the string
     */
    public static String translateSplit(String in) {
        if (in == null || in.length() < 1)
            return "";
        StringTokenizer strt = new StringTokenizer(in, " ");
        String out = new String();

        while (strt.hasMoreTokens())
            out = out + Utils.getResourceText(strt.nextToken(), true) + " ";

        return out.trim();
    }

    /**
     * Gets the plural of a string.
     *
     * @param input            the input
     * @return the plural
     */
    public static String getPlural(String input) {
        if (input.endsWith("y")) {
            return input.substring(0, input.length() - 1) + "ies";
        } else if (input.endsWith("s")) {
            return input + "es";
        } else {
            return input + "s";
        }
    }

    /**
     * Find resource by property.
     *
     * @param type
     *            the type
     * @param property
     *            the property
     * @param value
     *            the value
     * @return the resource
     */
    public static Resource findResourceByProperty(String type, String property, String value) {
        ResourceQuery query = new ResourceQuery(type);
        query.add(Criteria.Equal(type, property, value));
        ArrayList<Resource> found = getDatabase().getResources(query);

        if (found.size() > 0) {
            return found.get(0);
        } else
            return null;
    }

    /**
     * Find resource by property in database.
     *
     * @param database the database
     * @param type the type
     * @param property the property
     * @param value the value
     * @return the resource
     */
    public static Resource findResourceByPropertyInDatabase(AbstractDatabase<?> database, String type, String property,
            String value) {
        ResourceQuery query = new ResourceQuery(type);
        query.add(Criteria.Equal(type, property, value));
        ArrayList<Resource> found = database.getResources(query);

        if (found.size() > 0) {
            return found.get(0);
        } else
            return null;
    }

    /**
     * Generate http URL to webservice of the current context.
     *
     * @return the string
     */
    public static String generateUrlToWebservice() {
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext exContext = fc.getExternalContext();
        String servername = exContext.getRequestServerName();
        int port = exContext.getRequestServerPort();
        String appname = exContext.getRequestContextPath();
        String protocol = exContext.getRequestScheme();
        // String pagePath = exContext.getInitParameter("pagePath"); //read it
        // from web.xml

        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(protocol).append("://").append(servername);
        if (protocol.equals("http") && port == 80) {
            // don't touch url
        } else if (protocol.equals("https") && port == 443) {
            // don't touch url
        } else {
            urlBuilder.append(":").append(port);
        }
        urlBuilder.append(appname);
        return urlBuilder.toString();
    }

    /**
     * Gets the current url map.
     *
     * @return the current url map
     */
    public static HashMap<String, String> getCurrentURLMap() {
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext exContext = fc.getExternalContext();
        String servername = exContext.getRequestServerName();
        String port = String.valueOf(exContext.getRequestServerPort());
        String appname = exContext.getRequestContextPath();
        String protocol = exContext.getRequestScheme();
        String pagePath = exContext.getInitParameter("pagePath"); // read it
                                                                  // from
                                                                  // web.xml

        HashMap<String, String> theMap = new HashMap<String, String>();
        theMap.put("servername", servername);
        theMap.put("port", port);
        theMap.put("protocol", protocol);
        theMap.put("appname", appname);
        theMap.put("pagepath", pagePath);

        return theMap;
    }

    /**
     * Removes the final trail from url.
     *
     * @param url
     *            the url
     * @return the string
     */
    public static String removeFinalTrailFromURL(String url) {
        if (url != null && url.length() > 0 && url.charAt(url.length() - 1) == '/') {
            url = url.substring(0, url.length() - 1);
        }

        return url;
    }

    /**
     * Refresh view.
     */
    public static void refreshView() {
        FacesContext context = FacesContext.getCurrentInstance();
        String viewId = context.getViewRoot().getViewId();
        ViewHandler handler = context.getApplication().getViewHandler();
        UIViewRoot root = handler.createView(context, viewId);
        root.setViewId(viewId);
        context.setViewRoot(root);
    }

    /**
     * Fix mdr key for backend save. The char ':' used in MDRkeys is illegal in
     * JSF context for map entries, so we have to "fix" them to '__" instead.
     * Same about the char '.' which is just as illegal in JSF context for map
     * entries.
     *
     * So for saving we have to turn __ into : and _ into . again
     *
     * @param key
     *            the key
     * @return the string
     */
    public static String fixMDRkeyForSave(String key) {
        if (key.startsWith("urn")) {
            // MDR key, replace __ with : and _ with .
            key = key.replace("__", ":");
            key = key.replace("_", ".");
        }

        return key;
    }

    /**
     * Fix mdr key for backend save. The char ':' used in MDRkeys is illegal in
     * JSF context for map entries, so we have to "fix" them to '__" instead.
     * Same about the char '.' which is just as illegal in JSF context for map
     * entries.
     *
     * So for loading we have to turn : into __ and . into _
     *
     * @param key
     *            the key
     * @return the string
     */
    public static String fixMDRkeyForLoad(String key) {
        if (key.startsWith("urn")) {
            // MDR key, replace __ with : and _ with .
            key = key.replace(":", "__");
            key = key.replace(".", "_");
        }

        return key;
    }

    /**
     * Checks if a url is reachable with the given connector.
     *
     * @param httpConnector the http connector
     * @param url the url
     * @return true/false
     */
    public static Boolean checkHostReachable(HttpConnector43 httpConnector, String url) {
        // first check if we have an internet connection at all!
        try {
            URL testURL = new URL(url);
            HttpGet httpGet = new HttpGet(testURL.toURI());
            Integer port = testURL.getPort();
            if (port < 0)
                port = testURL.getDefaultPort();
            HttpHost target = new HttpHost(testURL.getHost(), port, testURL.getProtocol());
            HttpClientContext localContext = HttpClientContext.create();
            httpConnector.getHttpClient(target).execute(target, httpGet, localContext);
            return true;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Lowers the first char of a string.
     *
     * @param string the string
     * @return the string
     */
    public static String lowerCaseFirstChar(String string) {
        if (string == null || string.length() == 0) {
            return string;
        }

        char c[] = string.toCharArray();
        c[0] = Character.toLowerCase(c[0]);
        string = new String(c);

        return string;
    }

    /**
     * Uppers the first char of a string.
     *
     * @param string the string
     * @return the string
     */
    public static String upperCaseFirstChar(String string) {
        if (string == null || string.length() == 0) {
            return string;
        }

        char c[] = string.toCharArray();
        c[0] = Character.toUpperCase(c[0]);
        string = new String(c);

        return string;
    }

    /**
     * Adds a String item to a list that is in a hashmap under a certain key.
     *
     * @param theHashMap            The HashMap<String, List<String>>
     * @param theHashKey            The HashMap key under which the list shall be
     * @param theItemToAddToList            The item we want to add to that list
     * @param unique            If the item shall be unique in that list
     * @return theHashMap with the added item
     */
    public static HashMap<String, List<String>> addItemToListInHashMap(HashMap<String, List<String>> theHashMap,
            String theHashKey, String theItemToAddToList, Boolean unique) {
        List<String> theList = null;

        if (theHashMap.containsKey(theHashKey)) {
            theList = theHashMap.get(theHashKey);
            if (!unique || !theList.contains(theItemToAddToList)) {
                theList.add(theItemToAddToList);
            }
        } else {
            theList = new ArrayList<>();
            theList.add(theItemToAddToList);
        }
        theHashMap.put(theHashKey, theList);

        return theHashMap;
    }

    /**
     * Adds a String item to a list(LinkedHashSet) that is in a hashmap under a
     * certain key.
     *
     * @param theHashMap            The HashMap<String, LinkedHashSet<String>>
     * @param theHashKey            The HashMap key under which the list shall be
     * @param theItemToAddToList            The item we want to add to that list
     * @return theHashMap with the added item
     */
    public static HashMap<String, LinkedHashSet<String>> addItemToLinkedHashSetInHashMap(
            HashMap<String, LinkedHashSet<String>> theHashMap, String theHashKey, String theItemToAddToList) {
        LinkedHashSet<String> theList = null;

        if (theHashMap.containsKey(theHashKey)) {
            theList = theHashMap.get(theHashKey);
        } else {
            theList = new LinkedHashSet<>();
        }
        theList.add(theItemToAddToList);

        theHashMap.put(theHashKey, theList);
        return theHashMap;
    }

    /**
     * Parses based on a Google Closure .soy template to a string
     *
     * @param soyFile
     *            The .soy file
     * @param soyNamespace
     *            The SOY namespace
     * @param soyForm
     *            the SOY form name
     * @param data
     *            The SOY data
     * @return String the content of the newly created file
     */
    public static String parseSOY(String soyFile, String soyNamespace, String soyForm, SoyMapData data) {
        soyFile = Utils.getRealPath(soyFile);

        SoyFileSet sfs = SoyFileSet.builder().add(new File(soyFile)).build();
        SoyTofu tofu = sfs.compileToTofu();
        SoyTofu simpleTofu = tofu.forNamespace(soyNamespace);
        String fileContent = simpleTofu.newRenderer(soyForm).setData(data).render();
        return fileContent;
    }

    /**
     * Writes a file.
     *
     * @param targetDirectory            The directory the file shall be created in
     * @param fileName            the filename the file should have
     * @param fileContent            the fileContent to be written
     * @return File
     */
    public static File writeFile(String targetDirectory, String fileName, String fileContent) {
        String saveFileName = targetDirectory + fileName;
        saveFileName = Utils.getRealPath(saveFileName);

        File file = new File(saveFileName);
        if (file.exists()) {
            file.delete();
        }

        try {
            file.createNewFile();

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(fileContent);
            bw.close();
        } catch (IOException e) {
            Utils.getLogger().error("Writing of file " + saveFileName + " failed");
            e.printStackTrace();
        }

        return file;
    }

    /**
     * Validates a XML against a given XSD and returns the error handler Use
     * errorhandler.isValid() or read out the error messages provided by it
     *
     * @param xml            The XML
     * @param xsd            The XSD
     * @return XMLErrorHandler
     * @throws SAXException the SAX exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static XMLErrorHandler validateAgainstXSD(InputStream xml, InputStream xsd)
            throws SAXException, IOException {
        XMLErrorHandler errorHandler = new XMLErrorHandler();
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(new StreamSource(xsd));
        Validator validator = schema.newValidator();
        validator.setErrorHandler(errorHandler);
        validator.validate(new StreamSource(xml));
        return errorHandler;
    }

    /**
     * Returns the string value of a JSONResource entry, or null if it doesn't
     * exist.
     *
     * @param data the data
     * @param key the key
     * @return the string of json resource
     */
    public static String getStringOfJSONResource(JSONResource data, String key) {
        if (data.getProperty(key) == null)
            return null;

        return data.getProperty(key).getValue();
    }

    /**
     * Returns the boolean value of a JSONResource entry, or false if it doesn't
     * exist or isn't even a boolean so make sure you actually expect a boolean
     * here.
     *
     * @param data the data
     * @param key the key
     * @return the boolean of json resource
     */
    public static Boolean getBooleanOfJSONResource(JSONResource data, String key) {
        if (data.getProperty(key) == null || data.getProperty(key).getValue() == null)
            return false;

        if (data.getProperty(key).asBoolean() == null)
            return false;

        return data.getProperty(key).asBoolean();
    }

    /**
     * Checks if the config is set as being a bridgehead.
     *
     * @param config            a JSONResource config
     * @return the boolean
     */
    public static Boolean isBridgehead(JSONResource config) {
        String thePackage = getStringOfJSONResource(config, Vocabulary.Config.thePackage);

        if (thePackage == null || !thePackage.equalsIgnoreCase(Vocabulary.Config.Package.bridgehead))
            return false;
        else
            return true;
    }

    /**
     * Check if this application is a Bridgehead of OSSE.
     *
     * @param configuration            A apache configuration config
     * @return boolean
     */
    public static Boolean isBridgehead(Configuration configuration) {
        String thePackage = (String) configuration.getProperty("instance.package");

        if (thePackage == null)
            return false;

        if (thePackage.equalsIgnoreCase("bridgehead"))
            return true;

        return false;
    }

    /**
     * Displays a context message The given subjec and message can be either a
     * free string or a resource bundle key.
     *
     * @param subject            The subject of the message or a resource bundle key
     * @param message the message
     */
    public static void displayContextMessage(String subject, String message) {
        String messageSubject = "";
        String messageText = "";

        try {
            messageSubject = getResourceBundleString(subject);
        } catch (MissingResourceException e) {
            messageSubject = subject;
        }

        try {
            messageText = getResourceBundleString(message);
        } catch (MissingResourceException e) {
            messageText = message;
        }

        addContextMessage(messageSubject, messageText);
    }

    /**
     * Validates the password.
     *
     * @param password            The password
     * @param passwordRepeat            The repeated password
     * @param username            The username
     * @param lastname            The lastname
     * @return Errormessage or null if all is fine
     */
    public static String validatePassword(String password, String passwordRepeat, String username, String lastname) {
        if (!password.equals(passwordRepeat)) {
            return "error_newpasswords_not_same";
        } else if (password.equalsIgnoreCase(username)) {
            return "error_newpassword_is_username";
        } else if (password.equalsIgnoreCase(lastname)) {
            return "error_newpassword_is_lastname";
        } else {
            try {
                PasswordComplexityValidator.validatePassword(password, null);
            } catch (PasswordComplexityException ex) {
                return ex.getMessage();
            }
        }

        return null;
    }

}
