# Samply.EDC

Samply.EDC is the base library used for Samply.EDC.OSSE providing necessary classes to build own registry application.

# Build

In order to build this project, you need to configure maven properly. See
Samply.Maven for more information.

Use maven to build the jar:

``` 
mvn clean package
```

Use it as a dependency:

```xml
<dependency>
    <groupId>de.samply</groupId>
    <artifactId>edc</artifactId>
    <version>${version}</version>
</dependency>
```
